const express = require('express')
const redis = require('redis')

const app = express()
app.use(express.json())
const redisClient = redis.createClient({
    url: "redis://redis:6379"
})
// const redisClient = redis.createClient()

redisClient.on('connect', () => {
    console.log('Connected to Redis');
})

redisClient.on('error', (err) => {
    console.error('Redis error', err)
})

redisClient.connect()

app.get('/', async (req, res) => {
    const messages = await redisClient.lRange("messages", 0, -1)
    res.send(messages)
})

app.post('/', async (req, res) => {
    const message = req.body.message
    await redisClient.rPush("messages", message)
    res.send("Message added")
})

app.listen(3000, () => {
    console.log('Server listening on port 3000')
})